public class TPTester {

  public static void main(String[] args) {
    WildCat nganu = new WildCat("ALICE", 11, 10);
    TrainCar ngini = new TrainCar(nganu);

    WildCat ngene = new WildCat("BOB", 20, 10);
    TrainCar ngana = new TrainCar(ngene, ngini);

    System.out.println(ngini.computeTotalWeight());

    nganu.weight = 50;
    nganu.length = 150;

    System.out.printf("%.2f",ngini.computeTotalMassIndex());
    System.out.print("\n"+ngini.printCar());

    System.out.print("\n"+ngana.printCar());

    nganu.weight = 10;
    ngene.weight = 20;

    System.out.print("\n"+ngana.computeTotalWeight());
    System.out.print("\n"+nganu.weight+" "+nganu.length+" "+nganu.length*0.01);
    System.out.print("\n"+ngene.weight+" "+ngene.length+" "+ngene.length*0.01);
    System.out.printf("\n"+"%.2f",nganu.computeMassIndex());
    System.out.printf("\n"+"%.2f",ngana.computeTotalMassIndex());


  }

}

import java.util.Arrays;

public class SplitReplace {
	
	public static void main(String[] args) {
		
		String test = "This\",i\"s,an\",e\"xample,str\"ing";
		
		String[] splitLine = test.split(",");
		
		for (int i = 0; i < splitLine.length; i++) {
			splitLine[i] = splitLine[i].replace("\"", "");	
		}
		
		for (String w : splitLine) System.out.print(w + " ");
		
	}
}
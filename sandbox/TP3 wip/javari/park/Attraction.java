package javari.park;

import java.util.List;
import javari.animal.*;

public class Attraction implements SelectedAttraction {

    private String name;
    private String[] animalTypes;
    private List<Animal> performers;

    public Attraction(String name, String[] animalTypes, List<Animal> performers) {
        this.name = name;
        this.animalTypes = animalTypes;
        this.performers = performers;
    }

    public String getName() { return this.name; }

    public String[] getType() { return this.animalTypes; }

    public List<Animal> getPerformers() {  return this.performers; }

    public boolean addPerformer(Animal performer) {
        if (!this.performers.contains(performer)) {
            this.performers.add(performer);
            return true;
        }
        return false;
    }
}
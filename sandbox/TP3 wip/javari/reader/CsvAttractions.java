package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class CsvAttractions extends CsvReader {

    private List<String> validAttractions = Arrays.asList("Circles of Fires", "Dancing Animals", "Counting Masters",
            "Passionate Coders");

    public CsvAttractions(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (this.validAttractions.contains(line.split(COMMA)[1])) {
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (!this.validAttractions.contains(line.split(COMMA)[1])) {
                amount++;
            }
        }

        return amount;
    }
}
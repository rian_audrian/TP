package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class CsvSections extends CsvReader {

    protected List<String> validSections = Arrays.asList("Explore the Mammals", "World of Aves", "Reptillian Kingdom");

    public CsvSections(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (this.validSections.contains(line.split(COMMA)[2])) {
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (!this.validSections.contains(line.split(COMMA)[2])) {
                amount++;
            }
        }

        return amount;
    }
}
package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class CsvCategories extends CsvReader {

    protected List<String> validCategories = Arrays.asList("mammals", "aves", "reptiles");

    public CsvCategories(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (this.validCategories.contains(line.split(COMMA)[1])) {
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (!this.validCategories.contains(line.split(COMMA)[1])) {
                amount ++;
            }
        }

        return amount;
    }
}
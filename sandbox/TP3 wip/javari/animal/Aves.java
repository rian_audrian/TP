package javari.animal;

public class Aves extends Animal {

    protected boolean isLayingEggs;

    public Aves(Integer id, String type, String name, Gender gender, double length, double weight,
    boolean isLayingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = isLayingEggs;
    }

    public boolean specificCondition() {
        return !this.isLayingEggs;
    }
}
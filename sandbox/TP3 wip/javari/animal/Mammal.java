package javari.animal;

public class Mammal extends Animal {

    protected boolean isPregnant;

    public Mammal(Integer id, String type, String name, double length, double weight, Gender gender, 
    boolean isPregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = isPregnant;
    }

    public boolean specificCondition() {
        if (this.getType().equalsIgnoreCase("Lion")) {
            return this.getGender().equals("Male") && !this.isPregnant;
        } else {
            return !this.isPregnant;
        }
    }
}
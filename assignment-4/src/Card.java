import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.*;

public class Card extends JButton {

    //private final ArrayList<Image> icons;
    private int id;
    private boolean matched;

    public Card(int id, ImageIcon activeIcon, ImageIcon disabledIcon) {
        this.setIcon(activeIcon);
        this.setDisabledIcon(disabledIcon);
        this.id = id;
        this.matched = false;
    }

    public int getID() {
        return this.id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public boolean matches(Card c) {
        return this.getID() == c.getID();
    }

    public boolean isMatched() {
        return this.matched;
    }

    public void setMatched(boolean status) {
        this.matched = status;
    }
}
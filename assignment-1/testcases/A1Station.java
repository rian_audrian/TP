import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        TrainCar recent = null;

        // add the train cars
        int amountOfCars = Integer.parseInt(input.nextLine());
        int carsOnTrack = 0;
        for (int i = 0; i < amountOfCars; i++) {
          String[] command = input.nextLine().split(",");
          WildCat newCat = new WildCat(command[0], Double.parseDouble(command[1]), Double.parseDouble(command[2]));
          TrainCar newCar = new TrainCar(newCat);
          if (recent == null) {
            recent = newCar;
          } else {
            newCar.next = recent;
            recent = newCar;
          }
          carsOnTrack += 1;

          //Check total weight of all train cars
          if (recent.computeTotalWeight() >= 250) {
            depart(recent, carsOnTrack);         //Depart the train if total weight exceeds threshold
            recent = null;          //reset the most recent car, so there won't be redundant cars departing
            carsOnTrack = 0;      //count the new number of cars
          }

        }
        //Depart the train if there are any leftover cars in queue
        if (recent != null) {
          depart(recent, carsOnTrack);
        }
    }

    public static void depart(TrainCar recent, int amountOfCars) {
      double totalMassIndex = recent.computeTotalMassIndex();
      String massCategory = "";
      System.out.println("The train departs to Javari Park");
      System.out.print("[LOCO]<--");
      recent.printCar();
      System.out.println("Average mass index of all cats: " + String.format("%.2f", totalMassIndex/amountOfCars));
      if (totalMassIndex < 18.5) massCategory = "underweight";
      else if (totalMassIndex >= 18.5 && totalMassIndex < 25) massCategory = "normal";
      else if (totalMassIndex >= 25 && totalMassIndex < 30) massCategory = "overweight";
      else if (totalMassIndex >= 30) massCategory = "overweight";
      System.out.println("In average, the cats in the train are " + massCategory);
    }
}

import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import animal.*;
import cage.*;

public class JavariPark {

    public static void main(String[] args) {
        String[] species = {"cat", "lion", "eagle", "parrot", "hamster"};
        ArrayList<CageArrangement> allCages = new ArrayList<CageArrangement>();
        ArrayList<Animal[]> animals = new ArrayList<Animal[]>();

        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        for (int i = 0; i < species.length; i++) {
            System.out.println("\n"+species[i] + ": ");
            int amount = Integer.parseInt(input.nextLine());
            if (amount == 0) {
                continue;
            } else {
                String data = input.nextLine();
                Animal[] tempData = recordData(species[i], data, amount);
                animals.add(tempData);
                ArrayList<Animal> toCage = new ArrayList<Animal>(Arrays.asList(tempData));
                allCages.add(new CageArrangement(toCage));
                }
            }
        
        System.out.println("Animals have been successfully recorded!");
        System.out.println("=============================================");
        //display cage arrangement
        System.out.println("Cage arrangement:");

        for (int i = 0; i < 5; i++) {
            CageArrangement cages = allCages.get(i);
            System.out.println("Before rearrangement:");
            cages.printInfo();
            System.out.println("After rearrangement: ");
            cages.rearrangeCages();
            cages.printInfo();
            System.out.print("\n\n");
        }

        Boolean inSpeciesMenu = true;
        
        //todo visit animal
        while (inSpeciesMenu) {
        System.out.println("Which animal you want to visit?");
        System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit");
        String cmd = input.nextLine();
            if (cmd == "99") {
                System.out.println("Bye fren");
                break;
            } else if (Integer.parseInt(cmd) > 5) {
                System.out.println("You do nothing...\n");
                continue;
            } else {
                Integer speciesNumber = Integer.parseInt(cmd)-1;
                Animal[] speciesIndex = animals.get(speciesNumber);
                String speciesName = speciesIndex[speciesNumber];
                inSpeciesMenu = false;
            }
        }

        boolean inAnimalsMenu = true;

        while (inAnimalsMenu) {
            System.out.println("Mention the name of " + speciesName + " you want to visit: ");
            cmd = input.nextLine();

            Animal current = null;
            boolean hasAnimal = false;

            for (Animal a : speciesData) {
                if (a.getName().equals(cmd)) {
                    hasAnimal = true;
                    current = a;
                    break;
                }
            }
        }

        if (hasAnimal) {
            System.out.println("You are visiting " + cmd + " (" + speciesName + ") now, what would you like to do?");
            current.action();
        } else {
            System.out.println("There is no " + speciesName + " with that name! ");
        }

        System.out.println("Back to the office!\n");

        input.close();
    }

    public static Animal[] recordData(String species, String data, int amount) {
        Animal[] speciesData = new Animal[amount];
        String[] animalsToCreate = data.split(",");

        for (int i = 0; i < speciesData.length; i++) {
            String[] temp = animalsToCreate[i].split("\\|");
            String name = temp[0];
            int length = Integer.parseInt(temp[1]);
            if (species == "cat") {
                speciesData[i] = new Cat(name, length);
            } else if (species == "eagle") {
                speciesData[i] = new Eagle(name, length);
            } else if (species == "hamster") {
                speciesData[i] = new Hamster(name, length);
            } else if (species == "lion") {
                speciesData[i] = new Lion(name, length);
            } else if (species == "parrot") {
                speciesData[i] = new Parrot(name, length);
            } else {
                break;
            }
        }

        return speciesData;
        
    }
}
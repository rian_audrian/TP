package animal;
import java.util.Scanner;

public class Eagle extends Animal {

    public Eagle(String name, int length) {
        super(name, length);
        this.outdoor = true;
        this.species = "Eagle";
    }

    public action() {
        Scanner inp = new Scanner(System.in);
        int cmd = Integer.parseInt(inp.nextLine());

        if (cmd == 1) fly();
        else System.out.println("You do nothing...")
    }

    public void fly() {
        System.out.println(this.getName() + " makes a voice: kwaakk..");
        System.out.println("You hurt!");
    }
}
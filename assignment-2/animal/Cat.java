package animal;
import java.util.Random;
import java.util.Scanner;

public class Cat extends Animal {

    public Cat(String name, int length) {
        super(name, length);
        this.outdoor = false;
        this.species = "Cat";
    }

    public action() {
        Scanner inp = new Scanner(System.in);
        int cmd = Integer.parseInt(inp.next);

        if (inp == 1) brush();
        else if (inp == 2) cuddle();
        else System.out.println("You do nothing...");
    }

    public void brush() {
        System.out.println("Time to clean " + this.getName() + "'s fur");
        System.out.println(this.getName() + " makes a voice: Nyaaan...");
    }

    public void cuddle() {
        Random randomizer = new Random();
        int selection = randomizer.nextInt(3);

        String voice = ""; //What the cat will say
        if (selection == 0) {
            voice = "Miaaaw..";
        } else if (selection == 1) {
            voice = "Purrr..";
        } else if (selection == 2) {
            voice = "Mwaw!";
        } else {
            voice = "Mraaawr!";
        }

        System.out.println(this.getName() + " makes a voice: " + voice);
    }
}
package javari.park;

import java.util.List;

public class Section {

    private List<String> animalCategories;
    private String name;

    public Section(String name, List<String> animalCategories) {
        this.name = name;
        this.animalCategories = animalCategories;
    }

    public String getName() { return this.name; }

    public List<String> getAnimalCategories() { return this.getAnimalCategories(); }
}
